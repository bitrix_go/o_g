<?
$MESS["OPTIMAL_GROUP_MODULE_NAME"] = "Модуль тестового задания Optimalgroup";
$MESS["OPTIMAL_GROUP_MODULE_DESC"] = "Разработчик Константин Тарасенко";
$MESS["OPTIMAL_GROUP_PARTNER_NAME"] = "";
$MESS["OPTIMAL_GROUP_PARTNER_URI"] = "https://optimalgroup.ru/";

$MESS["OPTIMAL_GROUP_DENIED"] = "Доступ закрыт";
$MESS["OPTIMAL_GROUP_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["OPTIMAL_GROUP_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["OPTIMAL_GROUP_FULL"] = "Полный доступ";

$MESS["OPTIMAL_GROUP_INSTALL_TITLE"] = "Установка модуля";
$MESS["OPTIMAL_GROUP_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
?>