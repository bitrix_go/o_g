<?php
require($_SERVER["DOCUMENT_ROOT"]."/vendor/autoload.php");

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Type;
use \Optimal\Group\CurrencyTable;

class groupOrmAdd extends CBitrixComponent
{

    protected function checkModules()
    {
        if (!Main\Loader::includeModule('optimal.group'))
            throw new Main\LoaderException(Loc::getMessage('OPTIMAL_GROUP_MODULE_NOT_INSTALLED'));
    }

    //Добавление записи
    function addCurrencyRow()
    {

		$this->deleteAll();

		$client = new \GuzzleHttp\Client();

		$date_from = date('Y-m-d');
		$date_to = date('Y-m-d', strtotime('-30 days'));

		// Получаем котировки за последние 30 дней
		$res = $client->request('GET', 'https://api.exchangeratesapi.io/history?start_at='.$date_to.'&end_at='.$date_from.'&base=USD');

		$data_array = json_decode($res->getBody(), true);

		$res_count = 0;

		foreach($data_array["rates"] as $date_value => $currency_list){
			foreach($currency_list as $currency => $currency_value){
				$result = CurrencyTable::add(array(
					'CURRENCY_DATE' => new Type\DateTime(date('d.m.Y 00:00:00',strtotime($date_value))),
					'CURRENCY' => $currency,
					'CURRENCY_VALUE' => $currency_value
				));

				if ($result->isSuccess())
				{
					++$res_count;
				}
				else
				{
					$error=$result->getErrorMessages();
					$res_text .='Произошла ошибка при добавлении: <pre>'.var_export($error,true).'</pre>';
				}
			}

		}

		return (!empty($res_text)?$res_text:$res_count);
    }

	public static function deleteAll()
	{

		$connection = \Bitrix\Main\Application::getConnection();
		$connection->truncateTable(CurrencyTable::getTableName());

	}

    public function executeComponent()
    {
        $this -> includeComponentLang('class.php');

        $this -> checkModules();

        $result = $this->addCurrencyRow();

        $this->arResult = $result;

        $this->includeComponentTemplate();
    }
};