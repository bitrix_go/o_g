<?php
use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Type;
use \Optimal\Group\CurrencyTable;

class groupOrmGetlist extends CBitrixComponent
{

    protected function checkModules()
    {
        if (!Main\Loader::includeModule('optimal.group'))
            throw new Main\LoaderException(Loc::getMessage('OPTIMAL_GROUP_MODULE_NOT_INSTALLED'));
    }


	function validateDate($date, $format = 'd.m.Y')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) === $date;
	}

    function getCurrency($filter = array())
    {
		$filter_data = array();

		if(!empty($filter["DATE_FROM"]) && $this->validateDate($filter["DATE_FROM"]) && !empty($filter["DATE_TO"]) && $this->validateDate($filter["DATE_TO"]))
		{
			$filter_data = array(">=CURRENCY_DATE" => new \Bitrix\Main\Type\DateTime($filter["DATE_FROM"]), "<=CURRENCY_DATE" => new \Bitrix\Main\Type\DateTime($filter["DATE_TO"]));
		}
		elseif(!empty($filter["DATE_FROM"]) && $this->validateDate($filter["DATE_FROM"]))
		{
			$filter_data = array(">=CURRENCY_DATE" => new \Bitrix\Main\Type\DateTime($filter["DATE_FROM"]));
		}
		elseif(!empty($filter["DATE_TO"]) && $this->validateDate($filter["DATE_TO"]))
		{
			$filter_data = array("<=CURRENCY_DATE" => new \Bitrix\Main\Type\DateTime($filter["DATE_TO"]));
		}

        $result = CurrencyTable::getList(array(
            'select'  => array('ID','CURRENCY_DATE','CURRENCY','CURRENCY_VALUE'),
			'filter'  => $filter_data,
            'order'   => array('CURRENCY_DATE'=>'DESC'),
        ));

        return $result;
    }


    public function executeComponent()
    {

        $this -> includeComponentLang('class.php');

        $this -> checkModules();

        $result = $this->getCurrency($this->arParams);

        while ($row = $result->fetch())
        {
            $this -> arResult[] = $row;
        }


        $this->includeComponentTemplate();
    }
};