<?
	foreach($arResult as $key=>$value)
	{
		$all[$value["CURRENCY"]][$value["CURRENCY_DATE"]->format("Y-m-d")] = number_format((float)$value["CURRENCY_VALUE"], 2, '.', '');

		if(!in_array($value["CURRENCY_DATE"]->format("Y-m-d"),$dates))
		{
			$dates[] = $value["CURRENCY_DATE"]->format("Y-m-d");
		}

	}

	$arResult["ALL"] = $all;
	$arResult["DATES"] = $dates;
?>