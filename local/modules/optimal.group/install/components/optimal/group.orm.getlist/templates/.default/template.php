<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

	<table class="curtable">

		<tr>
			<th></th>
			<?foreach($arResult["DATES"] as $date):?>
				<th><?=$date?></th>
			<?endforeach?>
		</tr>

		<?foreach($arResult["ALL"] as $currency => $value):?>

		<tr>
			<td class="main_col"><?=$currency?></td>
			<?foreach($value as $date => $currency_value):?>
				<td><?=$currency_value?></td>
			<?endforeach?>
		</tr>

		<?endforeach?>
	</table>