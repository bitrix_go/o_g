<?php
namespace Optimal\Group;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class CurrencyTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'currency_group';
    }

    public static function getUfId()
    {
        return 'CURRENCY_GROUP';
    }

    /*public static function getConnectionName()
    {
        return 'default';
    }*/

    public static function getMap()
    {
        return array(
            //ID
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),

            //Дата котировки
            new Entity\DatetimeField('CURRENCY_DATE', array(
                'required' => true,
            )),

            //Название валюты
            new Entity\StringField('CURRENCY', array(
                'required' => true,
            )),
            //Котировка
            new Entity\FloatField('CURRENCY_VALUE', array(
                'required' => true,
            )),


        );
    }

}